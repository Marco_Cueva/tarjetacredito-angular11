1.INSTALAR VISUAL CODE
2.INSTALAR NODE JS 13.14(�LTIMO PARA WINDOWS 7)
verificar en cmd: node -v 
Se instala node para poder usar el npm(gestor de paquetes)
3.INSTALAR TYPESCRIPT
EN CMD: npm install -g typescript
---------------------------------
4.INSTALAR ANGULAR CLI
EN CMD:npm install -g @angular/cli
verificar :ng --version 
otra forma de verificar: ng -v
Importante: siempre verificar si las librerias de npm son compatibles con la versi�n del angular
---------------------------------
5.CREAR PROYECTO: ng new angular-HolaMundo (crear carpetas,instala paquetes para habilitar el desarrollo)
6. Para correr el proyecto: ng server desde origen de Proyecto
servidor local por defecto : http://localhost:4200/
-si se desea entrar por otro puerto : ng serve --port 4201
7.Crear Componentes : ng g c nombreComponente ->Ejm: ng g c components/TarjetaCredito
8.Crear rutas, agregar en app.module.ts:
-import { Routes, RouterModule } from '@angular/router';
-const routes: Routes = [
  { path: 'contacto', component: ContactoComponent },
  { path: 'nosotros', component: NosotrosComponent },
  { path: 'equipo/:id', component: EquipoComponent },
  { path: '',   component:InicioComponent, pathMatch: 'full' },
  { path: '**',   redirectTo: '/', pathMatch: 'full' }
];
-imports: [
    RouterModule.forRoot(routes)//agregado en import
  ], 
---------------------------------------------  
9.Crear Servicio: ng g s nombreServicio 
-recomiendan eliminar spec.ts generado
10.Agregar bootstrap: 
-ng add @ng-bootstrap/ng-bootstrap  (se necesita para crear un modal, esto funciona solo sin las siguientes)
-npm install bootstrap --save
-npm install jquery
-npm install popper.js --save
-referencia en html:<link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.css">
11.Trabajar con formularios,agregar en app.module.ts: 
-import { ReactiveFormsModule } from '@angular/forms';
-imports: [
    ReactiveFormsModule
  ],
-agregar en el componente a afectar: import { FormGroup } from '@angular/forms';  
--------------------------------------------
12.Trabajar con FireBase : ng add @angular/fire   y  npm install firebase @angular/fire
-Dar "y" para autorizar, te manda al navegador para seleccionar la cuenta de google,
copiar y pegar las keys de acceso.
-instalar con comando: npm install -g firebase-tools
-agregar en environment.prod.ts : 
export const environment = {
  firebase: {
    apiKey: "AIzaSyBqUscPgdkrxnGrdnCXczwcblL7U_7xaW0",
    authDomain: "fir-app-3a889.firebaseapp.com",
    databaseURL: "https://fir-app-3a889.firebaseio.com",
    projectId: "fir-app-3a889",
    storageBucket: "fir-app-3a889.appspot.com",
    messagingSenderId: "362962161607",
    appId: "1:362962161607:web:1c340530d83d7124acd3e2"
  }
};
-agregar en app.module.ts: 
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from 'src/environments/environment.prod';

imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule
  ]
-en comando para seleccionar el proyecto firebase: firebase use --add
(escoger su id del proyecto)
-correr proyecto: ng run nombreProyecto:deploy
--------------------------------------------------------------------
13.Para hacer paginaci�n de un listado:
-npm i ngx-pagination --save
--------------------------------------------------------------------
14.luego de crear el proyecto y correr por primera vez,Agregar bootstrap
->npm install bootstrap jquery @popperjs/core
Luego, en el archivo �angular.json�, deber�s poner las siguientes instrucciones en los objetos �styles� y �scripts�:

    "styles": [
    "node_modules/bootstrap/dist/css/bootstrap.min.css",
    "src/styles.scss"
    ],
    "scripts": [
    "node_modules/jquery/dist/jquery.min.js",
    "node_modules/@popperjs/core/dist/umd/popper.min.js",
    "node_modules/bootstrap/dist/js/bootstrap.min.js"
    ]
Luego verificar con algunas clases de bootstrap y correr con "ng serve"
----------------------------------------------------------------------
recompilar: ng serve --o
----------------------------------------------------------------------
15.Agregar Toastr para mostrar mensajes de exito
fuente: https://www.npmjs.com/package/ngx-toastr
->(verificar la compatibiidad entre versiones de angular y ngx-toastr)
->npm install ngx-toastr@13.2.1 --save		(compatible con angular 11)
->npm install @angular/animations@12.2.16 --save 	(conpatible con angular 11)
Luego n el archivo �angular.json deber�s poner las siguientes instrucciones en los objetos �styles�
"styles": [
  "styles.scss",
  "node_modules/ngx-toastr/toastr.css" // try adding '../' if you're using angular cli before 6
]

Luego agregar en app.module.ts :
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
  ],
 
})

Luego agregar en la clase tuComponente.ts :
import { ToastrService } from 'ngx-toastr';

@Component({...})
export class YourComponent {
  constructor(private toastr: ToastrService) {}

  showSuccess() {
    this.toastr.success('Hello world!', 'Toastr fun!');
  }
}
----------------------------------------------------------------------
instalar y usar el  common para trabajar httpclient y servicios en angular(common@11.0.9 es compatible con angular 11)
----------------------------------------------------------------------










